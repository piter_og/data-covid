<?php

use App\Http\Controllers\Api\Authentication\VersionGateway\UserLoginGatewayController;
use App\Http\Controllers\Api\Authentication\VersionGateway\UserLogoutGatewayController;
use App\Http\Controllers\Api\Authentication\VersionGateway\UserMeGatewayController;
use App\Http\Controllers\Api\Authentication\VersionGateway\UserRegisterGatewayController;
use App\Http\Controllers\VersionGateway\CountryGatewayController;
use App\Http\Controllers\VersionGateway\CovidCaseGatewayController;
use App\Http\Controllers\VersionGateway\SubscriberGatewayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::lapiv(function () {
    Route::prefix('/auth')->group(function () {
        Route::post('register', UserRegisterGatewayController::class);
        Route::post('login', UserLoginGatewayController::class);
    });

    Route::middleware('auth:sanctum')->group(function() {
        Route::prefix('/auth')->group(function() {
            Route::post('logout', UserLogoutGatewayController::class);
            Route::post('me', UserMeGatewayController::class);
        });

        Route::post('cases/create', [CovidCaseGatewayController::class, 'store'])->name('cases.store');
    });

    Route::get('cases', [CovidCaseGatewayController::class, 'index'])->name('cases.index');
    Route::get('cases/region', [CovidCaseGatewayController::class, 'region'])->name('cases.index');
    Route::get('countries', [CountryGatewayController::class, 'index'])->name('countries.index');
    Route::get('countries/{country}', [CountryGatewayController::class, 'show'])->name('countries.show');

    Route::post('subscribe', [SubscriberGatewayController::class, 'subscribe']);
    Route::post('unsubscribe', [SubscriberGatewayController::class, 'unsubscribe']);
});


