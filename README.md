# DataCovid
 Project to test Laravel features and packages;
 
 All covid data contained here are fake, this project there only one objective, test laravel features and my abilities with it;
 
## Versions
 - PHP 8.0.7
 - Laravel 8.50.0

## Features
 - Sail
 - Cache
 - Mail
 - Sanctum
 - Minio
 - Localization Language

## Packages
 - [Laravel Sail](https://laravel.com/docs/8.x/sail)
 - [LAPIV - Api version](https://github.com/juliomotol/lapiv)
 - [Guzzle](https://docs.guzzlephp.org/en/stable/) 
 - [AWS Flysystem](https://github.com/thephpleague/flysystem-aws-s3-v3)
 - [EloquentFilter](https://github.com/Tucker-Eric/EloquentFilter)

## External API
 - [Rest Countries](https://restcountries.eu)

---------------
## Commands
`sail artisan datac19:install`

`sail artisan datac19:fill` 

## API Versions

### V1 

### V2
