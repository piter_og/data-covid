<?php

namespace App\Observers;

use App\Models\CovidCase;
use Illuminate\Support\Facades\Cache;

class CovidCaseObserver
{
    /**
     * Handle the CovidCase "created" event.
     *
     * @param CovidCase $covidCase
     * @return void
     */
    public function created(CovidCase $covidCase)
    {
        $forgetList = [
            'covidCasesByCountry',
            'covidCasesByRegion'
        ];

        foreach ($forgetList as $item) {
            Cache::forget($item);
        }
    }
}
