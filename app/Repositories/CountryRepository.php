<?php

namespace App\Repositories;

use App\Models\Country;
use App\Repositories\Contracts\CountryRepositoryInterface;
use http\Env\Request;

class CountryRepository extends Repository implements CountryRepositoryInterface
{
    protected $model;

    public function __construct(Country $model)
    {
        $this->model = $model;
    }
}
