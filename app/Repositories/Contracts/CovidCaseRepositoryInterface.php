<?php

namespace App\Repositories\Contracts;

interface CovidCaseRepositoryInterface
{
    public function sumCasesByCountry();

    public function sumCasesByRegion();

    public function getTotalCasesByCountry(int $country_id): int;

    public function getTotalCasesByRegion(string $region): int;
}
