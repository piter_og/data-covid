<?php

namespace App\Repositories\Contracts;

interface SubscriberRepositoryInterface
{
    /**
     * @param string $email
     * @param string $token
     * @return int
     */
    public function findSubscription(string $email, string $token);
}
