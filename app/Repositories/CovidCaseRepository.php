<?php

namespace App\Repositories;

use App\Models\CovidCase;
use App\Repositories\Contracts\CovidCaseRepositoryInterface;
use Illuminate\Support\Facades\DB;

class CovidCaseRepository extends Repository implements CovidCaseRepositoryInterface
{
    protected $model;

    public function __construct(CovidCase $model)
    {
        $this->model = $model;
    }

    public function sumCasesByCountry()
    {
        return $this->model->selectRaw("SUM(number) as number, country_id, MAX(created_at) as created_at")
            ->groupBy('country_id')
            ->get();
    }

    public function sumCasesByRegion()
    {
        return $this->casesByRegion()->get();
    }

    public function getTotalCasesByCountry(int $country_id): int
    {
        return $this->model->where('country_id', '=', $country_id)->sum('number');
    }

    public function getTotalCasesByRegion(string $region): int
    {
        if ($cases = $this->casesByRegion()->where('region', $region)->first()) {
            return $cases->number;
        }

        return 0;
    }

    protected function casesByRegion()
    {
        return $this->model->join('countries', 'covid_cases.country_id', '=', 'countries.id')
            ->groupBy('countries.region')
            ->select([DB::raw('SUM(covid_cases.number) as number'), 'countries.region']);
    }
}
