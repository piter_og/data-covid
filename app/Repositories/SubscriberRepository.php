<?php

namespace App\Repositories;

use App\Models\Subscriber;
use App\Repositories\Contracts\SubscriberRepositoryInterface;

class SubscriberRepository extends Repository implements SubscriberRepositoryInterface
{
    protected $model;

    public function __construct(Subscriber $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $email
     * @param string $token
     * @return int
     */
    public function findSubscription(string $email, string $token)
    {
        return $this->model
            ->where('email', '=', $email)
            ->where('token', '=', $token)
            ->first();
    }
}
