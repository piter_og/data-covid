<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriberRequest;
use App\Http\Requests\UnsubscribeRequest;
use App\Http\Resources\SubscriberV1Resource;
use App\Repositories\Contracts\SubscriberRepositoryInterface;
use App\Services\Contracts\SubscriberServiceInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Response;

class SubscriberV1Controller extends Controller
{
    use ApiResponserTrait;

    public function __construct(
        private SubscriberRepositoryInterface $mainRepository,
        private SubscriberServiceInterface $subscriberService
    ) {}

    /**
     * @param SubscriberRequest $request
     * @return Response
     */
    public function subscribe(SubscriberRequest $request)
    {
        $data = $request->validated();
        $data['token'] = $this->subscriberService->generateToken($data['email']);

        $collection = new SubscriberV1Resource($this->mainRepository->create($data));

        return $this->success($collection);
    }


    /**
     * @param UnsubscribeRequest $request
     * @return mixed
     */
    public function unsubscribe(UnsubscribeRequest $request)
    {
        $data = $request->validated();

        if (! $this->subscriberService->canUnsubscribe($data)) {
            return $this->error("Email and token did not match our records.", 403, $data);
        }

        $subscription = $this->mainRepository
            ->findSubscription($data['email'], $data['token']);

        $this->mainRepository->delete($subscription->id);

        return $this->success(null, 'Email has been unsubscribed', 200);
    }
}
