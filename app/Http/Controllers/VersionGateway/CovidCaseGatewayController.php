<?php

namespace App\Http\Controllers\VersionGateway;

use App\Http\Controllers\CovidCaseV1Controller;
use App\Http\Controllers\CovidCaseV2Controller;
use JulioMotol\Lapiv\GatewayController;

class CovidCaseGatewayController extends GatewayController
{
    protected $apiControllers = [
        CovidCaseV1Controller::class,
        CovidCaseV2Controller::class,
    ];
}
