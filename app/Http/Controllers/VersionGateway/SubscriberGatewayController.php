<?php

namespace App\Http\Controllers\VersionGateway;

use App\Http\Controllers\SubscriberV1Controller;
use JulioMotol\Lapiv\GatewayController;

class SubscriberGatewayController extends GatewayController
{
    protected $apiControllers = [
        SubscriberV1Controller::class
    ];
}
