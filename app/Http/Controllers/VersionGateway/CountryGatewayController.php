<?php

namespace App\Http\Controllers\VersionGateway;

use App\Http\Controllers\CountryV1Controller;
use JulioMotol\Lapiv\GatewayController;

class CountryGatewayController extends GatewayController
{
    protected $apiControllers = [
        CountryV1Controller::class,
    ];
}
