<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCaseByCountryV2Resource;
use App\Repositories\Contracts\CovidCaseRepositoryInterface;
use App\Services\Contracts\CovidCasesCacheServiceInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Response;

class CovidCaseV2Controller extends Controller
{
    use ApiResponserTrait;

    public function __construct(
        private CovidCaseRepositoryInterface $mainRepository,
        private CovidCasesCacheServiceInterface $covidCasesCacheService
    )
    { }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cases = $this->covidCasesCacheService->getCasesByCountry();

        $collection = CovidCaseByCountryV2Resource::collection($cases);

        return $this->success($collection);
    }
}
