<?php

namespace App\Http\Controllers;

use App\Http\Requests\CovidCaseRequest;
use App\Http\Resources\CovidCaseByCountryV1Resource;
use App\Http\Resources\CovidCaseByRegionV1Resource;
use App\Http\Resources\CovidCaseV1Resource;
use App\Repositories\Contracts\CovidCaseRepositoryInterface;
use App\Services\Contracts\CovidCasesCacheServiceInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CovidCaseV1Controller extends Controller
{
    use ApiResponserTrait;

    public function __construct(
        private CovidCaseRepositoryInterface $mainRepository,
        private CovidCasesCacheServiceInterface $covidCasesCacheService
    )
    { }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cases = $this->covidCasesCacheService->getCasesByCountry();

        $collection = CovidCaseByCountryV1Resource::collection($cases);

        return $this->success($collection);
    }

    /**
     * @return mixed
     */
    public function region()
    {
        $cases = $this->covidCasesCacheService->getCasesByRegion();

        $collection = CovidCaseByRegionV1Resource::collection($cases);

        return $this->success($collection);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(CovidCaseRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = Auth::id();

        $collection = new CovidCaseV1Resource($this->mainRepository->create($data));
        return $this->success($collection);
    }
}
