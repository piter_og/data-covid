<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryV1Resource;
use App\Models\Country;
use App\Repositories\Contracts\CountryRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Request;

class CountryV1Controller extends Controller
{
    use ApiResponserTrait;

    const API_PAGINATE = 15;

    public function __construct(private CountryRepositoryInterface $mainRepository)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $data = $this->mainRepository->filter($request->all());

        $collection = CountryV1Resource::collection($data->paginate(self::API_PAGINATE));

        return $this->success($collection);
    }

    public function show($apiVersion, $country)
    {
        $collection = new CountryV1Resource($this->mainRepository->find($country));

        return $this->success($collection);
    }

}
