<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserLoginV1Controller extends Controller
{
    use ApiResponserTrait;

    /**
     * UserLoginController constructor.
     * @param UserRepositoryInterface $mainRepository
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository
    ) {
    }

    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function __invoke(UserLoginRequest $request)
    {
        $payload = $request->validated();

        if (! Auth::attempt($payload)) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success([
           'token' => auth()->user()->createToken('API Token')->plainTextToken
        ], 'User logged successfully');
    }
}
