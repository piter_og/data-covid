<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserMeV1Controller extends Controller
{
    use ApiResponserTrait;

    /**
     * UserMeController constructor.
     * @param $
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function __invoke()
    {
        return $this->success(auth()->user());
    }
}
