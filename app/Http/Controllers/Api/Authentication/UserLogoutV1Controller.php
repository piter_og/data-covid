<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserLogoutV1Controller extends Controller
{
    use ApiResponserTrait;

    /**
     * UserLogoutController constructor.
     * @param UserRepositoryInterface $mainRepository
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository,
    ) {}

    /**
     * Handle the incoming request.
     *
     * @param  Request  $request
     * @return Response
     */
    public function __invoke()
    {
        auth()->user()->tokens()->delete();

        return $this->success([], 'Tokens Revoked');
    }
}
