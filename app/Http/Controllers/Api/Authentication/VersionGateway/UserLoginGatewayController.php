<?php

namespace App\Http\Controllers\Api\Authentication\VersionGateway;

use App\Http\Controllers\Api\Authentication\UserLoginV1Controller;
use JulioMotol\Lapiv\GatewayController;

class UserLoginGatewayController extends GatewayController
{
    protected $apiControllers = [
        UserLoginV1Controller::class,
    ];
}
