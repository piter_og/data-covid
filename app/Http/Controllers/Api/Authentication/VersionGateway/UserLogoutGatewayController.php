<?php

namespace App\Http\Controllers\Api\Authentication\VersionGateway;

use App\Http\Controllers\Api\Authentication\UserLogoutV1Controller;
use JulioMotol\Lapiv\GatewayController;

class UserLogoutGatewayController extends GatewayController
{
    protected $apiControllers = [
        UserLogoutV1Controller::class,
    ];
}
