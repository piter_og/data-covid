<?php

namespace App\Http\Controllers\Api\Authentication\VersionGateway;

use App\Http\Controllers\Api\Authentication\UserMeV1Controller;
use JulioMotol\Lapiv\GatewayController;

class UserMeGatewayController extends GatewayController
{
    protected $apiControllers = [
        UserMeV1Controller::class,
    ];
}
