<?php

namespace App\Http\Controllers\Api\Authentication\VersionGateway;

use App\Http\Controllers\Api\Authentication\UserRegisterV1Controller;
use JulioMotol\Lapiv\GatewayController;

class UserRegisterGatewayController extends GatewayController
{
    protected $apiControllers = [
        UserRegisterV1Controller::class,
    ];
}
