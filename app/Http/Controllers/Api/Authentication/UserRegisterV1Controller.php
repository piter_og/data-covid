<?php

namespace App\Http\Controllers\Api\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegisterRequest;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Hash;

class UserRegisterV1Controller extends Controller
{
    use ApiResponserTrait;

    /**
     * UserRegisterController constructor.
     * @param UserRepositoryInterface $mainRepository
     */
    public function __construct(
        private UserRepositoryInterface $mainRepository
    ) {}

    public function __invoke(UserRegisterRequest $request)
    {
        $payload = $request->validated();
        $payload['password'] = Hash::make($payload['password']);

        $user = $this->mainRepository->create($payload);

        return $this->success([
            'user' => $user,
            'token' => $user->createToken('API Token', ['user:manage', 'product:manage'])->plainTextToken
        ], 'User created successfully!');
    }

}
