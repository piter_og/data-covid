<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CovidCaseV1Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cases' => $this->number,
            'country' => $this->country->name,
            'report_at' => $this->created_at->format('Y-m-d'),
            'report_by' => $this->user->email ?? null,
        ];
    }
}
