<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CovidCaseByCountryV2Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total_cases' => $this->number,
            'country' => $this->country->name,
            'last_update_at' => $this->created_at->format('Y-m-d H:i')
        ];
    }
}
