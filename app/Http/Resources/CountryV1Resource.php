<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CountryV1Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $host = config('app.url').':9000/'.config('filesystems.disks.s3.bucket').'/';

        return [
            'id' => $this->id,
            'name' => $this->name,
            'alpha_code' => $this->alpha_code,
            'region' => $this->region,
            'code' => $this->numeric_code ?? null,
            'flag' => $host.$this->flag
        ];
    }
}
