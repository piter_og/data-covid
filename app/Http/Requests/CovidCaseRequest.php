<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CovidCaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => ['required', 'integer', 'min:0', 'max:99999999'],
            'country_id' => ['required', 'integer', 'exists:countries,id'],
            'user_id' => ['nullable', 'integer', 'exists:users,id'],
        ];
    }
}
