<?php

namespace App\Providers;

use App\Services\Contracts\SubscriberServiceInterface;
use App\Services\SubscriberService;
use Illuminate\Support\ServiceProvider;

class SubscriberServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubscriberServiceInterface::class, SubscriberService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
