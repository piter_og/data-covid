<?php

namespace App\Providers;

use App\Services\Contracts\CovidCasesCacheServiceInterface;
use App\Services\CovidCasesCacheService;
use Illuminate\Support\ServiceProvider;

class CovidCaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CovidCasesCacheServiceInterface::class, CovidCasesCacheService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
