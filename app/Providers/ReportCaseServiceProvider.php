<?php

namespace App\Providers;

use App\Services\Contracts\ReportCaseServiceInterface;
use App\Services\ReportCaseService;
use Illuminate\Support\ServiceProvider;

class ReportCaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ReportCaseServiceInterface::class, ReportCaseService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
