<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\{SubscriberRepository,CovidCaseRepository,CountryRepository,UserRepository, };
use App\Repositories\Contracts\{SubscriberRepositoryInterface,CovidCaseRepositoryInterface,CountryRepositoryInterface,UserRepositoryContract, UserRepositoryInterface};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubscriberRepositoryInterface::class, SubscriberRepository::class);
        $this->app->bind(CovidCaseRepositoryInterface::class, CovidCaseRepository::class);
        $this->app->bind(CountryRepositoryInterface::class, CountryRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
