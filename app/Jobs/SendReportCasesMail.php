<?php

namespace App\Jobs;

use App\Mail\CasesReportMail;
use App\Services\Contracts\ReportCaseServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendReportCasesMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param ReportCaseServiceInterface $reportCaseService
     * @return void
     */
    public function handle(ReportCaseServiceInterface $reportCaseService)
    {
        $details = $reportCaseService->getReportData();

        foreach ($details as $detail) {
            $email = new CasesReportMail($detail);
            Mail::to($detail['email'])->send($email);
        }
    }
}
