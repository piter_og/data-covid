<?php

namespace App\Console\Commands;

use Database\Seeders\CovidCaseSeeder;
use Illuminate\Console\Command;

class DataCovidFillCases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datac19:fill {amount?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill some Covid Cases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $amount = $this->argument('amount') ?? 500;

        for ($i=0; $i < $amount; $i++) {
            $this->call(CovidCaseSeeder::class);
        }

        $this->info("$amount covid cases generated successfully!");
    }
}
