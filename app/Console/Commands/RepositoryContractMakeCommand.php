<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryContractMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository-contract {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository contract';

    /**
     * The type of command
     *
     * @var string
     */
    protected $type = 'RepositoryContract';

    /**
     * Replace the class name to stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        return str_replace('GenericRepositoryInterface', $this->argument('name'), $stub);
    }

    /**
     * Get the stub file.
     *
     * @return string
     */
    protected function getStub()
    {
        return  'stubs/repository-contract.stub';
    }

    /**
     * Get default namespace to class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Repositories\Contracts';
    }

    /**
     * Get arguments from console command.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the repository contract.'],
        ];
    }
}
