<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class DataCovidInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datac19:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install project and seed the countries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (App::environment('production')) {
            $this->info('Only run this command on local environment!');
            return;
        }

        $this->info('Installing, wait a moment!');
        $bar = $this->output->createProgressBar(6);
        $bar->start();

        $this->callSilent('migrate:refresh');
        $this->nextStep($bar);
        $this->callSilent('db:seed', ['class' => 'CountrySeeder']);
        $this->nextStep($bar);
        $this->callSilent('cache:clear');
        $this->nextStep($bar);
        $this->callSilent('config:clear');
        $this->nextStep($bar);
        $this->callSilent('view:clear');
        $this->nextStep($bar);
        $this->callSilent('storage:link');
        $this->nextStep($bar);
        $bar->finish();

        $this->info("");
        $this->info("System successfully installed!");
        $this->info("Now, you can register a new user to insert new covid cases.");
    }

    public function nextStep($bar)
    {
        $bar->advance();
    }
}
