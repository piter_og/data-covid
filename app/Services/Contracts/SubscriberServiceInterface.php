<?php

namespace App\Services\Contracts;

interface SubscriberServiceInterface
{
    /**
     * @param array $content
     * @return bool
     */
    public function canUnsubscribe(Array $content);

    /**
     * @param String $content
     * @return mixed
     */
    public function generateToken(String $content);
}
