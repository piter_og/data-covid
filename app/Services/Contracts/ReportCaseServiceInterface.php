<?php

namespace App\Services\Contracts;

interface ReportCaseServiceInterface
{
    public function getReportData();
}
