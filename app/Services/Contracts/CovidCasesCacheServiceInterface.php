<?php

namespace App\Services\Contracts;

interface CovidCasesCacheServiceInterface
{
    /**
     * @return mixed
     */
    public function getCasesByCountry();

    /**
     * @return mixed
     */
    public function getCasesByRegion();
}
