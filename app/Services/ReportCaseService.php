<?php

namespace App\Services;

use App\Repositories\Contracts\CovidCaseRepositoryInterface;
use App\Repositories\Contracts\SubscriberRepositoryInterface;
use App\Services\Contracts\ReportCaseServiceInterface;
use Illuminate\Support\Facades\Hash;

class ReportCaseService implements ReportCaseServiceInterface
{
    /**
     * @var SubscriberRepositoryInterface
     */
    private $subscriberRepository;
    /**
     * @var CovidCaseRepositoryInterface
     */
    private CovidCaseRepositoryInterface $covidCaseRepository;

    /**
     * SubscriberService constructor.
     * @param SubscriberRepositoryInterface $subscriberRepository
     * @param CovidCaseRepositoryInterface $covidCaseRepository
     */
    public function __construct(
        SubscriberRepositoryInterface $subscriberRepository,
        CovidCaseRepositoryInterface $covidCaseRepository
    ) {
        $this->subscriberRepository = $subscriberRepository;
        $this->covidCaseRepository = $covidCaseRepository;
    }

    public function getReportData()
    {
        $reportList = $this->subscriberRepository->all();
        $covidCaseRepository = $this->covidCaseRepository;

        return $reportList->map(function ($item, $key) use ($covidCaseRepository) {
            return [
                'email' => $item->email,
                'name' => $item->name,
                'country' => [
                    'name' => $item->country->name,
                    'total' => $covidCaseRepository->getTotalCasesByCountry($item->country_id)
                ],
                'region' => [
                    'name' => $item->country->region,
                    'total' => $covidCaseRepository->getTotalCasesByRegion($item->country->region)
                ]
            ];
        });
    }
}
