<?php

namespace App\Services;

use App\Repositories\Contracts\CovidCaseRepositoryInterface;
use App\Services\Contracts\CovidCasesCacheServiceInterface;
use Illuminate\Support\Facades\Cache;

class CovidCasesCacheService implements CovidCasesCacheServiceInterface
{
    const CACHE_TIME = 60*60*24; // 24 hours

    /**
     * @var CovidCaseRepositoryInterface
     */
    private $mainRepository;

    public function __construct(CovidCaseRepositoryInterface $mainRepository)
    {
        $this->mainRepository = $mainRepository;
    }

    /**
     * @return mixed
     */
    public function getCasesByCountry()
    {
        return Cache::remember('covidCasesByCountry', self::CACHE_TIME, function () {
            return $this->mainRepository->sumCasesByCountry();
        });
    }

    /**
     * @return mixed
     */
    public function getCasesByRegion()
    {
        return Cache::remember('covidCasesByRegion', self::CACHE_TIME, function () {
            return $this->mainRepository->sumCasesByRegion();
        });
    }
}
