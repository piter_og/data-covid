<?php

namespace App\Services;

use App\Repositories\Contracts\SubscriberRepositoryInterface;
use App\Services\Contracts\SubscriberServiceInterface;
use Illuminate\Support\Facades\Hash;

class SubscriberService implements SubscriberServiceInterface
{
    /**
     * @var SubscriberRepositoryInterface
     */
    private $subscriberRepository;

    /**
     * SubscriberService constructor.
     * @param SubscriberRepositoryInterface $subscriberRepository
     */
    public function __construct(
        SubscriberRepositoryInterface $subscriberRepository
    ) {
        $this->subscriberRepository = $subscriberRepository;
    }

    /**
     * @param String $content
     * @return mixed|string
     */
    public function generateToken(string $content)
    {
        return Hash::make($content);
    }

    /**
     * @param array $content
     * @return bool|void
     */
    public function canUnsubscribe(array $content)
    {
        $subscribe = $this->subscriberRepository
            ->findSubscription($content['email'], $content['token']);

        return (bool) $subscribe;
    }
}
