<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CasesReportMail extends Mailable
{
    use Queueable, SerializesModels;

    private $content;

    /**
     * Create a new message instance.
     *
     * @param $content
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('report@covid.com', 'Report')
            ->subject('Your daily report about Covid19')
            ->markdown('emails.report-cases')
            ->with([
                'name' => $this->content['name'],
                'country_cases' => $this->content['country'],
                'region_cases' => $this->content['region']
            ]);
    }
}
