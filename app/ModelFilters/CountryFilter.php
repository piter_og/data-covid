<?php

namespace App\ModelFilters;

use App\Models\Country;
use App\Repositories\Contracts\CountryRepositoryInterface;
use App\Repositories\CountryRepository;
use EloquentFilter\ModelFilter;

class CountryFilter extends ModelFilter
{
    protected $mainRepository;

    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function alpha($id)
    {
        return $this->where('alpha_code', $id);
    }

    public function code($id)
    {
        return $this->where('numeric_code', $id);
    }

    public function s($search)
    {
        return $this->where('name', 'LIKE', "%$search%");
    }

    public function region($search)
    {
        return $this->where('region', 'LIKE', "%$search%");
    }

}
