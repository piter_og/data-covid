<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\CovidCase;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class CovidCaseSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CovidCase::create([
            'number' => rand(0, 100000),
            'country_id' => Country::all()->random()->id
        ]);
    }
}
