<?php

namespace Database\Seeders;
use App\Repositories\Contracts\CountryRepositoryInterface;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CountrySeeder extends Seeder
{
    const COUNTRY_API_URL = 'https://restcountries.eu/rest/v2';

    public function __construct(private CountryRepositoryInterface $countryRepository)
    { }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $request = $client->get(self::COUNTRY_API_URL.'/all');
        $response = json_decode($request->getBody());

        foreach ($response as $item) {
            $url = $item->flag;
            $imageContent = file_get_contents($url);
            $imageName = substr($url, strrpos($url, '/') + 1);
            Storage::disk('s3')->put($imageName, $imageContent);

            $this->countryRepository->create([
                'name' => $item->name,
                'alpha_code' => $item->alpha2Code,
                'region' => $item->region,
                'numeric_code' => $item->numericCode,
                'flag' => $imageName
            ]);
        }
    }
}
