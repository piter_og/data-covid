<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\CovidCase;
use Illuminate\Database\Eloquent\Factories\Factory;

class CovidCaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CovidCase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'number' => $this->faker->randomNumber(6),
            'country_id' => Country::factory()->create()->id
        ];
    }
}
