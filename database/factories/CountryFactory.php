<?php

namespace Database\Factories;

use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class CountryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Country::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // the city method was used to not limit this factory if the
        // table is already populated

        return [
            'name' => $this->faker->unique()->city(),
            'alpha_code' => $this->faker->unique()->lexify('??'),
            'region' => $this->faker->currencyCode(),
            'numeric_code' => $this->faker->unique(true, 50000)->numerify('#####'),
            'flag' => $this->faker->slug().'.svg'
        ];
    }
}
