@component('mail::message')
<h1> Covid Cases </h1>

Hello, **{{$name}}**

Below, you can check the cases of Covid-19 in your region.

<h2> {{$country_cases['name']}} </h2>

- Total: {{$country_cases['total']}}

<h2> Region: {{$region_cases['name']}} </h2>

- Total: {{$region_cases['total']}}

---------------------

Thank you

@endcomponent
